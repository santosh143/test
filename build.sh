#!/bin/bash
mvn clean package
STATUS=$?
if [ $STATUS -eq 0 ]; then
   docker version
   #mvn org.sonarsource.scanner.maven:sonar-maven-plugin:sonar       
echo "Deployment Successful"
ls target
docker build -t firefox-image .
docker images
docker-compose run run -i firefox-image
firefox -v
else
echo "Deployment Failed"
exit 1
fi
